<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin - edit {{$category->title}}</title>
</head>
<body>
<h1>Edit Category - {{$category->title}}</h1>
{!! Form::model($category, ['method' => 'PATCH', 'url' => 'skills/' . $category->id]) !!}

<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::submit('Update Category', ['class' => 'btn btn-primary form-control']) !!}
</div>

{!! Form::close() !!}
</body>
</html>