<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Create category</title>
</head>
<body>
    <h1>Add Category</h1>
{!! Form::open(array('action' => 'CategoriesController@store', 'class' => 'createcategory')) !!}

    <div class="row large-12 columns createcategory">
        {!! Form::label('title', 'title:') !!}
        {!! Form::text('title', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-12 columns">
        {!! Form::label('content', 'Detail:') !!}
        {!! Form::textarea('content', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-4 columns">
        {!! Form::submit('create category', ['class' => 'button']) !!}
    </div>

{!! Form::close() !!}

    </body>
</html>

