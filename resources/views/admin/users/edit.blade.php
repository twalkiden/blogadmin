<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <title>Edit- {{$users->name}}</title>
</head>
<body>
<h1>Edit- {{$users->name}}</h1>

<!-- form goes here -->
{!! Form::model($users, ['method' => 'PATCH', 'url' => '/admin/users/' . $users->id]) !!}

<div>
    {!! Form::label('name', 'Username:') !!}
    {!! Form::text('name', null) !!}
</div>

<div>
    {!! Form::label('email', 'Email Address:') !!}
    {!! Form::textarea('email', null) !!}
</div>

<div>
    {!! Form::label('roles', 'Roles:') !!}
    @foreach($roles as $role)
        {{ Form::label($role->name) }}
        {{ Form::checkbox('role[]', $role->id, $users->roles->contains($role->id), ['id' => $role->id]) }}
    @endforeach

</div>

<div>
    {!! Form::submit('Update User and Roles') !!}
</div>


{!! Form::close() !!}

</body>
</html>