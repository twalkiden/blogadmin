<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
   <title>New Roles</title>
</head>
<body>

<h1>New Role</h1>

<section>
    @if (isset ($roles))

        <ul>
            @foreach ($roles as $role)
                <li><a href="/admin/roles/{{ $role->id }}" name="{{ $role->title }}">{{ $role->title }}</a></li>
            @endforeach
        </ul>
    @else
        <p> no articles added yet </p>
    @endif
</section>


{{ Form::open(array('action' => 'RoleController@create', 'method'=> 'get')) }}
<div class="row">
    {!! Form::submit('Add Role', ['class' => 'button']) !!}
</div>
{{ Form::close(['']) }}
</body>
</html>