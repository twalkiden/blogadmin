<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create Role</title>
</head>
<body>
    <h1>Add Role </h1>
    {!! Form::open(array('action' => 'RoleController@store', 'id' => 'createrole')) !!}
    {{ csrf_field()}}

    <div class="row large-12 columns">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('Name', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-12 columns">
        {!! Form::label('label', 'lable:') !!}
        {!! Form::textarea('lable', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-4 columns">
        {!! Form::submit('Add Role', ['class' => 'button']) !!}
    </div>
    {!! Form::close() !!}
</body>
</html>