<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{$article->title }}</title>
</head>
<body>
    <h1>{{$article->title}}</h1>
    <p>{{$article->content}}</p>
    <p>creator: {{$article->user->name}}</p>
    <p>categories:</p>
    @if (isset ($article->categories))

        <ul>
            @foreach ($article->categories as $category)
                <li><p>{{ $category->title }}</p></li>
            @endforeach
        </ul>
    @else
        <p> no categories linked yet </p>
    @endif
</body>
</html>