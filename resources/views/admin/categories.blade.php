<!DOCTYPE html>
<html>
<head>
    <title>Laravel</title>
</head>
<body>
<div class="container">
    <div class="content">
        <div class="title">Categories</div>
    </div>
</div>
<h1>Categories</h1>

<section>
    @if (isset ($categories))

        ********************
            @foreach ($categories as $category)
                <h1>New Category added!</h1>
                <li><a href="/admin/categories/{{ $category->id }}" name="{{ $category->title }}">{{ $category->title }}</a></li>
            @endforeach
        ********************
    @else
        <p> no articles added yet </p>
    @endif
</section>

{{ Form::open(array('action' => 'CategoriesController@create', 'method' => 'get')) }}
<div class="row">
    {!! Form::submit('Add Category', ['class' => 'button']) !!}
</div>
{{ Form::close() }}
</body>
</html>