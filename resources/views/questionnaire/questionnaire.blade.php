{{--
    -- this page is used to show the questionnaire that people can take part in
    --section content tell the web browser werere to put the text within the master layout
    --this is connect to the ViewQuestionnaireController
   --}}

@extends('questionnaire.layouts.master')
@section('title', 'view a questionnaire')

@section('content')
    <article>
        <h1>Questionnaire available to fill out</h1>

        {{--
            --the if statment is used to make sure that their are questionnaires that can be answered
            -- the isset get the questionnaire table from the controller
            -- the foreach is used to gather each of the questionnaires avaliable
            -- li is used to list them
            --<a creates a link that acts as a way to create a url
            -- it grabes the id and the questionnaire table which will then be able to be shown on the next page
            -- the questionnaire_title is used to create a view for the link
            -- else is used to create a statment that show when their no questionnaire to be answered
        --}}

        <section>
            @if (isset ($questionnaire))
                <table>
                    <tr>
                        <th>Questionnaire Name</th>
                        <th>Ethics</th>
                        <th>Take part</th>
                    </tr>
                    <tr>

                        @foreach($questionnaire as $questionnaires)
                            <td>{{$questionnaires->questionnaire_title}}</td>
                            <td>{{$questionnaires->ethics}}</td>
                            <td>  <div class="form-button">
                                    <a href="/questionnaire/questionnaire/{{$questionnaires->id}}"  class="button">Take part </a>
                                </div></td>

                        @endforeach


                    </tr>

                </table>
            @else
                <p>No questionnaire available at the moment</p>
            @endif
        </section>
    </article>
@endsection