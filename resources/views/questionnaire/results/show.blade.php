{{--
    -- this page is used to show the results
    --this page has been accessed from results page
    --this page is inserted into the layout created in master
    --section content tell the web browser werere to put the text within the master layout
    --$questionnaire- is a varaible called from the controller
    -- the part after the varaibale is the column from the table that needs to be shown
    -- foreach is used to get all the question that belong to the questionnaire by the ids
    --$question-> question get the questions
    --$question-> creates link between question and answer in the results table
    -- then retreves the answers

--}}

@extends('questionnaire.layouts.master')
@section('title', 'results')
@section('content')

    <article>
        <h1>{!! $questionnaire->questionnaire_title !!}</h1>
        @foreach($questionnaire->questions as $question)
            <h2>{{$question->question}}</h2>
            @foreach($question->results as $answer)
                <li>{{$answer->answer}}</li>
            @endforeach
        @endforeach

    </article>
@endsection