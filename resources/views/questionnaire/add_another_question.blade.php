{{--
    --this page creates another question after the first question been made
    -- this extendes the layout which include the styling
--}}

@extends('questionnaire.layouts.master')
@section('title', 'Create a question')

{{--
    --The form is used to submit the question through the QuestionControler@store
    --this has a hidden input which is used to submit the questionnaire id which is being transfered from the first question
    -- the first input for the user is question-> which has the id of question to now what filed it is being put into
    -- a null value so that they have a option what to put in and class for styling
    -- the next is answer field which is three each with the same id but with [] makes them an array
    -- null is used to allows anything to put in with the same class as the other input box
    -- the button calls the controller to carry out the save
    -- the other button is used when they dont want to submit a new questionb
    -- the only bug is this not connected to the controller so needs to make sure they submit the question before clicking the button
    --takes them to the home page
--}}
@section('content')
    <article>
        <h1>Create a question</h1>
        <h2>Lets add another question to the questionnaire</h2>
        {!! Form::open(array('action'=> 'QuestionController@store', 'id'=> 'createquestion')) !!}

        {!! Form::hidden('questionnaire_id', $questionnaire, ['class'=> 'form-control']) !!}

        <div class="form-group">
            {!! Form::label('question', 'Added a question?') !!}
            {!! Form::text('question', null, ['class'=> 'form-control']) !!}

            {!! Form::label('answer', 'Added a answers?') !!}

            {!! Form::text('answer[]', null, ['class'=> 'form-control']) !!}
            {!! Form::text('answer[]', null, ['class'=> 'form-control']) !!}
            {!! Form::text('answer[]', null, ['class'=> 'form-control']) !!}
        </div>

        <div class="form-button">
            <input type="submit" id="submit" name="submit" value="Submit question" class="button">
        </div>

        <div class="form-button">
        <a href="/questionnaire/home">Exit creating questionnaire- make sure you add the question first</a>
        </div>

        {!! Form::close() !!}
        </article>
@endsection