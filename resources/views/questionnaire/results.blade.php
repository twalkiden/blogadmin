{{--
    -- this page is used to show the questionnaire that people have results to
    -- this is own shown when the user logged in
    --section content tell the web browser werere to put the text within the master layout
    --this is connect to the ViewresultsController
   --}}

@extends('questionnaire.layouts.master')
@section('title', 'view a questionnaire')

@section('content')
    <article>
        <h1>Results of questionnaires</h1>

        {{--
           --the if statment is used to make sure that their are questionnaires that have been created
           -- the isset get the questionnaire table from the controller
           -- the foreach is used to gather each of the questionnaires avaliable
           -- li is used to list them
           --<a creates a link that acts as a way to create a url
           -- it grabes the id and the questionnaire table which will then be able to be shown on the next page
           -- the questionnaire_title is used to create a view for the link
           -- else is used to create a statment that show when their no questionnaire to seen
       --}}
        <h3>This page is  for you to be able to see the results from the questionnaies that have been created</h3>
        <section>
            @if (isset ($questionnaire))
                <table>
                    <tr>
                        <th>Questionnaire Name</th>
                        <th>Ethics</th>
                        <th>See results</th>
                    </tr>
                    <tr>

                        @foreach($questionnaire as $questionnaires)
                            <td>{{$questionnaires->questionnaire_title}}</td>
                           <div class="hidden">{{$test = \App\Questionnaire::findOrFail($questionnaires->id)}}</div>
                            <td>@foreach($test->questions as $questions)
                            <li>{{$questions->question}}</li>
                            @endforeach</td>
                            <td>  <div class="form-button">
                                    <a href="/questionnaire/results/{{$questionnaires->id}}"  class="button">Results </a>
                                </div></td>
                             @endforeach


                    </tr>

                </table>
            @else
                <p>No questionnaire available at the moment</p>
            @endif
        </section>



    </article>
@endsection