{{--
    --The use of the master creates the same formate for  each of the pages
    -- this is created within html
    -- the link to the css- make sure each page get the same layout and formats
    -- @include calls in the navigation page so each page has the same nav options
    -- @yield content is used to indicate where the content on the other pages will be placed
   --@include footer make sure each page has a footer
--}}

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Questionnaire 4 you</title>
    <link rel="stylesheet" href="/css/app.css">
</head>
<body>
    <div class="layout">
        <h1>
         Questionnaire 4 You
        </h1>
    <header>
        @include('questionnaire.layouts.header')
    </header>
    <article>
        @yield('content')
    </article>

</div>

</body>
@include('questionnaire.layouts.footer')
</html>