{{--
    --this creates the navigation bar at the top of the page
    --the first three <a are the link avaliable at all times
    -- the if statement is used to add and remove certian nav depending if they loggged in or not
    --Auth::guest is the extra links unregistered user can see
    --else is the links user who have regiseted can see
--}}

<nav>
      <a href="/questionnaire/home">Home</a>
      <a href="/questionnaire/new_questionnaire">Create Questionnaire</a>
      <a href="/questionnaire/questionnaire">Take part</a>


      @if (Auth::guest())
            <a href="{{ url('/login') }}">Login</a>
            <a href="{{ url('/register') }}">Register</a>
      @else
            <a href="/questionnaire/results">See results</a>
            <a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a>


      @endif
</nav>