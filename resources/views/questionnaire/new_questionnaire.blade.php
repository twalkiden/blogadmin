{{--
    -- this page is used to show the user how to create a questionnaire
    --section content tell the web browser werere to put the text within the master layout
    -- this page is connect to the questionnaire controller
    -- this poage can not be accessed if the user is not log in
   --}}
@extends('questionnaire.layouts.master')
@section('title', 'Create a questionnaire')

@section('content')
    <article>
        <h1>Create a questionnaire</h1>
        <h2>This is your chance to name your questionnaire make sure that it tells the audience the topic.
            As well you can add a ethic statement which should include:</h2>
        <li>What you are trying to find out</li>
        <li>Contact details if people want to ask questions</li>
        <li>Also mention if the questionnaire might ask private or sensitive questions</li>

        {{--
            -- the form calls the questionnaireController@store
            -- the first input is hidden which is used to connect the questionnaire to the user
            -- it has the id of author_id telling the controller to place the id into the author_id coloumn
            -- input is set so the user can not change and it taken from the log in page when they login
        --}}
        {!! Form::open(array('action'=> 'QuestionnaireController@store', 'id'=> 'createquestionnaire')) !!}

        <div class="form-group">
            {!! Form::hidden('author_id',Auth()->user()->id, ['class'=> 'large-8 column']) !!}
        </div>

        {{--
            --the next formgroup allow the user to add a questionnair_title
            -- the label is the question they will be asked
            -- form::text is used as an input filed to connect tell them to input the data into the questionnaire_title column
        --}}

        <div class="form-group">
            {!! Form::label("question", 'What would you like to name your questionnaire?') !!}
            {!! Form::text('questionnaire_title', null, ['class'=> 'large-8 column']) !!}

        </div>

        {{--
            --the next formgroup allow the user to add a ethics statment
            -- the label is the question they will be asked
            -- form::text is used as an input filed to connect tell them to input the data into the ethics column
        --}}

        <div class="form-group">
            {!! Form::label("question", 'Added a Ethics statement to make sure your audience now the risks and what they are getting into') !!}
            {!! Form::textarea('ethics', null, ['class'=> 'form-control']) !!}
        </div>

        {{--
            --the next formgroup allow the user to submit the data
            -- form::submit is used to call the controller to save all the input fields
            -- then transfer them to the page where they can add questions
         --}}

        <div class="form-group">
            {!! Form::submit('Lets add the questions', ['class'=>'button']) !!}
        </div>
        {!! Form::close() !!}
    </article>
@endsection