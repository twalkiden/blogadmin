{{--
    -- this page is used to show the home page
    --section content tell the web browser werere to put the text within the master layout
   --}}

@extends('questionnaire.layouts.master')
@section('title', 'Home page')

@section('content')
    <h1>Home page</h1>
    <h2>Welcome to Questionnaire for You where you have chance to create your own questionnaires</h2>
    <h3>Their are many advantages of using this system which are:</h3>

    <table style="width:50%">
        <tr>
            <th>Features</th>
            <th>Registered users</th>
            <th>non-registered users</th>
        </tr>
        <tr>
            <td>Create questions</td>
            <td>Yes</td>
            <td>No</td>
        </tr>
        <tr>
            <td>See all questionnaire</td>
            <td>Yes</td>
            <td>yes</td>
        </tr>
        <tr>
            <td>See results to questionnaires</td>
            <td>Yes</td>
            <td>No</td>
        </tr>
        <tr>
            <td>Unlimited number of questionnaire creations</td>
            <td>Yes</td>
            <td>No</td>
        </tr>
        <tr>
            <td>Unlimmited number questionnaire you can take part in</td>
            <td>Yes</td>
            <td>Yes</td>
        </tr>

    </table>
@endsection