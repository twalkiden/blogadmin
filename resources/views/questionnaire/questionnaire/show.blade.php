{{--
    -- this page is used to show the form to answer questionnaires
    --this page has been accessed from questionnaire page
    --this page is inserted into the layout created in master
    -- this page is linked to the view questionnaire controller except section between form
    --section content tell the web browser werere to put the text within the master layout
   --}}

@extends('questionnaire.layouts.master')
@section('title', 'answer a questionnaire')
@section('content')

    {{--
        --$questionnaire is a variable inside the controller which recieve the data from the table
        --the two column being taken is the title and ethics
    --}}
 <article>
     <h1>{!! $questionnaire->questionnaire_title !!}</h1>
     <p>{!! $questionnaire->ethics !!}</p>
     <p>To answer the questions you need to unclick the ones you disagree with</p>

     {{--
             --$the form is connected to the ViewResultsController@store
             --which is used to save the data through the $request varable in the controler
             --their is two foreach the first is used to select all the questions linked to the table
             --this allows their to be a hidden input of questionnaire_id to link the table in the database
             -- in the development this will be explained in more detail why
             --the second is to get all the answer that are releated to the question
             -- then put them into a check box which allows the user to select mutiply answer
             -development process explain why they have been chosen
             --at the end of the foreach one submit button is used to call the controller to store the information
             --and redirect them
         --}}
     {!! Form::open(array('action'=> 'ViewResultsController@store', 'id'=> 'createresult')) !!}
     @foreach($questionnaire->questions as $question)
     <div class="form-group">
       <h2>{{$question->question}}</h2>
         {!! Form::hidden('question_id[]', $question->id, ['class'=> 'form-control']) !!}
             @foreach($question->answers as $answer)
             <div class="form-group">

           <h3>{!! Form::label('answer',$answer->answer) !!}
            {!! Form::checkbox('answer[]', $answer->answer, ['class'=> 'form-control'])!!}</h3>
         </div>
         @endforeach
     </div>
     @endforeach
   <div class="form-button">
         <input type="submit" id="submit" name="submit" value="submit answers" class="button">
     </div>
     {!! Form::close() !!}

 </article>
@endsection
