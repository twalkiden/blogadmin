<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questions extends Model
{
    /*
     * creates the model which links the controller to the questions tables
     * $fillable-> sets out the filed within the table that need to be manual filled in
     */
    protected $fillable = [
        'questionnaire_id',
        'question',

    ];
    /*
     * question_questionnaire function creates the link to the questionnaire table
     * this tells us that a question belongs to one questionnaire
     */
    public function question_questionnaire() {
        return $this->belongsTo('App\Questionnaire');
    }

    /*
    * answer function creates the link to the answer table
    * this tells us that a question belongs to many answers
    */


    public function answers() {
        return $this->belongsToMany('App\Answer');
    }

    /*
       * results function creates the link to the results table
       * this tells us that a question can have many results
       */

    public function results() {
        return $this->hasMany('App\Results');
    }
}
