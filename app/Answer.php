<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    /*
     * creates the model which links the controller to the answers tables
     * $fillable-> sets out the filed within the table that need to be manual filled in
     */
    protected $fillable = [
        'answer',
        'questions_id'

    ];

    /*
     * questions()-> creates the link between the answer and question table
     * tells us that a answer can belong to many questions
     */
    public function questions() {
        return $this->hasOne('App\Question');
    }


}
