<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question_Questionnaire extends Model
{
    public function question(){
        return $this->belongsToMany('App\Question'
        );}
    public function questionnaire(){
        return $this->belongsToMany('App\Questionnaire'
        );}
}
