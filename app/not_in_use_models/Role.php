<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function permissions(){
        return $this->belongsToMany(Permission::class);
    }
/*
  * use to sync methods and save methods
  * gives the role the permissions and adds to the role
  */
    public function givePermissionTo(Permission $permission){
        return $this->permissions()->sync($permission);
    }
}
