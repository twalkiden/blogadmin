<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Results extends Model
{
    /*
     * creates the model which links the controller to the results tables
     * $fillable-> sets out the filed within the table that need to be manual filled in
     */
    protected $fillable = [
      'answer',
      'questions_id'
    ];

    /*
       * answer function creates the link to the questions table
       * this tells us that each user answer belongs to one question
       */

    public function answer() {
        return $this->hasOne('App\Questions');
    }
}
