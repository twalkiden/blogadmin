<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
 * Routes page is used to connect each of the pages to correct controller
 */





Route::resource('/questionnaire/questionnaire', 'ViewQuestionnaireController');

Route::group(['middleware' =>['web']],function (){
    Route::auth();
    Route::get('/questionnaire/home', 'HomeController@index');
    Route::resource('/questionnaire/new_questionnaire', 'QuestionnaireController');
    Route::resource('/questionnaire/results', 'ViewResultsController');
    Route::resource('/questionnaire/new_question', 'QuestionController');
    Route::resource('/questionnaire/new_answer', 'AnswerController');
    });



