<?php

namespace App\Http\Controllers;

use App\Answer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Http\Requests;
use App\Questionnaire;
use App\Results;



class ViewQuestionnaireController extends Controller
{
    /*
     * new controller- view questionnaire
     * Aim- to allow user to answer questionnaire that have been made
     */

    public function index()
    {
        /*
         * Index- used to create a view which shows the list of questionnaires
         * $questionnaire-> request all the data from the questionnaire module which gets it from the table
         * return view creates a link to the page 'questionnaire/questionnaire'
         * 'questionnaire' used a variable within the view -> linking to the variable in this page
         * $questionnaire-> allows all the data from the table to be passed over
         */

        $questionnaire = Questionnaire::all();
        return view('questionnaire/questionnaire', ['questionnaire' => $questionnaire]);
    }

    public function show($id)
    {
        /*
         * $show used to create the page where the user answers the questionnaire
         * $id-> is the id extracted from the questionnaire chosen
         * $questionnaire -> take the questionnaire table and make sure the $id is present
         * $questionnaire->questions is used to create a direct link to the questions table to extract the questions
         * return the view of the form to fill out the questionnaire, which the varaiables to extract data from the
         * both  questions table and questionnaire table
         */


      $questionnaire = Questionnaire::findOrFail($id);
      $questionnaire->questions;
      return view('/questionnaire/questionnaire/show', ['questionnaire'=>$questionnaire] );


    }




}
