<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Http\Requests;
use App\Questionnaire;
use App\Results;


class ViewResultsController extends Controller
{
    /*
     * creates controller-> view results
     * purpose to view the results for each of the questionnaires
     */
    public function index()
    {
        /*
         * creates a function index which used to return view of the results page
         * $questionnaire-> request all the data from the questionnaire table
         * pass the data to a variable with in the page called 'questionnaire'
         */

        $questionnaire = Questionnaire::all();
        return view('questionnaire/results', ['questionnaire' => $questionnaire]);
    }



    public function store(Request $request)
    {
        /*creates the function store from results/show page form
         *$request all the input names being used with in the form
         * creates an array to store the response
         * first foreach used to get all the input fields name questions
         * second foreach used to get all the input field within that question name answer
         * $list_results-> links ito to the correct model and table causing it to make a new entry
         * 'answer'-> take the input field answer and put inside the answer column
         * 'questions_id'-> take the input field of the question and gives it the question id
         * save()-> saves everything within the array to the table
         * return them to see other questionnaire that they can answer
         */


        $list_of_results = [];
       foreach ($request->input('question_id', []) as $question_id){
       foreach($request->input('answer', []) as $results) {
             $list_of_results = Results::create([
                'answer' => $results,
                'questions_id' => $question_id
                ]);
        }}

        $list_of_results->save();
        return redirect('/questionnaire/questionnaire');

    }

    public function show($id)
    {

        $questionnaire = Questionnaire::findOrFail($id);
        $questionnaire->questions;

        return view ('/questionnaire/results/show',['questionnaire'=>$questionnaire]);



    }



}
