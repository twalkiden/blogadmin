<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends Controller
{
/*
 * creates the home page view
 */
    public function index()
    {
        return view('questionnaire/home');
    }
}
