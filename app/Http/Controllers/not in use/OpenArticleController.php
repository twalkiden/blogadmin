<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Article;

class OpenArticleController extends Controller
{
    public function index()
    {
        // get all the articles
        $articles = Article::with('user', 'categories')->get(); // should not do this on frontend.


        //return response()->json(['data' => $articles], 200); // step one seeing the data returned as json with no collection name.
        return response()->json([
           'data' => $this->transformCollection($articles)
        ], 200);
    }


    /*
     * Get the article and return it as json.
     *
     * Return it via the transform method to cleanse and restrict.
     *
     * Also send response code back.
     */
    public function show($id)
    {


        $articles = Article::with('user', 'categories')->where('id', $id)->first();

       // return $article; // step 1 shows all data with linked data as sub-collections.

       if(!$articles){
           return response()->json([
              'error' => ['message' => 'Article does not exist']
           ], 404);

       }
       return response()->json([
           'data' => $this->transform( $articles)
       ], 200);

    }

/*
 * Take the collection and separate out as individual articles to pass through the transform method.
 */
private function transformCollection($articles)
{
    return array_map([$this, 'transform'], $articles->toArray());

}
    /*
     * transform AN article to restrict fields and change the true DB column names.
     *
     * article author shows how we access joined tables data.
     */
    private
    function transform($article)
    {
        return [
            'article_id' => $article['id'],
            'article_title' => $article['title'],
            'article_body' => $article['content'],
            'article_author' => $article['user']['name'],
            //'article_author' => $article->user->name,   this is an alternative method to achieve the same as line above.
        ];
    }
}