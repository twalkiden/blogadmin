<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Gate;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\Role;
use App\Category;
class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
       if (Gate::allows('see_all_users')){
            $roles = Role::all();
            return view('admin/role', ['roles' => $roles]);
        }
        return view('/home');
    }

    public function edit($id)
    {
        $user = User::where('id',$id)->first();
        $roles = Role::all();

        if(!$user){
            return redirect('/admin/users');
        }
        return view('admin/roles/edit')->with('roles', $roles);
    }

    public function create()
    {
        $cats = Category::lists('title', 'id');
        return view('admin/roles/create', compact('cats'));
    }

}
