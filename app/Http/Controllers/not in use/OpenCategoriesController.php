<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\category2;

class OpenCategoriesController extends Controller
{
    public function index()
    {
        // get all the articles
        $category = category2::with('user', 'categories')->get(); // should not do this on frontend.


        //return response()->json(['data' => $articles], 200); // step one seeing the data returned as json with no collection name.
        return response()->json([
           'data' => $this->transformCollection($category)
        ], 200);
    }


    /*
     * Get the article and return it as json.
     *
     * Return it via the transform method to cleanse and restrict.
     *
     * Also send response code back.
     */
    public function show($id)
    {


        $category = Category2::with('user', 'categories')->where('id', $id)->first();

       // return $article; // step 1 shows all data with linked data as sub-collections.

       if(!$category){
           return response()->json([
              'error' => ['message' => 'category does not exist']
           ], 404);

       }
       return response()->json([
           'data' => $this->transform( $category)
       ], 200);

    }

/*
 * Take the collection and separate out as individual articles to pass through the transform method.
 */
private function transformCollection($category)
{
    return array_map([$this, 'transform'], $category->toArray());

}
    /*
     * transform AN article to restrict fields and change the true DB column names.
     *
     * article author shows how we access joined tables data.
     */
    private
    function transform($category)
    {
        return [
            'article_id' => $category['id'],
            'article_title' => $category['title'],
            'article_body' => $category['content'],
            'article_author' => $category['user']['name'],
            //'article_author' => $article->user->name,   this is an alternative method to achieve the same as line above.
        ];
    }
}