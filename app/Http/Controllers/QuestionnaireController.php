<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Http\Requests;
use App\Questionnaire;


class QuestionnaireController extends Controller
{
    /**
     * Aim of this page is to create the code which carries out three function
     * QuestionnaireController constructor.
     *shows a page to create questionnaire
     * shows a list of questionnaires
     * store a new questionnaire
     */
    public function __construct()
    {
        /**
         * This is the code that loads the auth
         * which means that they can not load the page without login in
         */
        $this->middleware('auth');
    }
    /*
        * Index - function
        * used to load a form to enter questionnaire
    */
    public function index()
    {
        return view('questionnaire/new_questionnaire');
    }

 /*
  * Public function store
  * $request-> data extracted from the new_questionnaire page
  * $questionnaire-> used to tell what the extracted data should do
  * Questionnaire:: -> the model which stores what should be extracted
  * with the create, $request->all() make sure that all the column in the database will be filled
  * save() -> submits the data to the database
  * return view used to take the user to a page to create question
  * passing the variable allows data to be stored like ids, name which can be used in the next form
  */
    public function store(Request $request)
    {
    $questionnaire = Questionnaire::create($request->all());
    $questionnaire->save();


     return view('/questionnaire/new_question', ['questionnaire'=> $questionnaire]);
    }
}
