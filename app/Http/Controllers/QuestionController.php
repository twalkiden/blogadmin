<?php

namespace App\Http\Controllers;

use App\Answer;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Questions;
use Illuminate\Support\Facades\Input;


class QuestionController extends Controller
{
    /*
     * The aim of this page is to create store functions of the questions
     * This controller does not have a index due to it lead from QuestionnaireController@store
     */
    public function store(Request $request)
    {
        /*
         * Store function used to save one question with three answers
         * $request-> gathers the data from the form
         * $questions-> links the controller to the model to make sure all the column need will be filled in
         * 'question'-> is one of the column in the question table
         * $request->input('question')-> is used to search with in  a form the label question and extract the values
         * this is the same for the line $questionnaire_id
         */

        $questions = Questions::create([
            'question' => $request->input('question'),
            'questionnaire_id' => $request->input('questionnaire_id'),
            $questionnaire_id = $request->input('questionnaire_id')

        ]);

        /*The next section creates the input for the three answers
         *$list_of_answer -> is array of the answer inputs
         * foreach is used to loop over the form and look for he input field of 'answers'
         * $answer is a one of the input filed answer- overall their is three
         * if statement-> used to catch to see if the field is empty and if it is ignore
         *The array is re-called within the for loop
         *The array receive the value of all the column need within the answer model
         * 'answer'-> is the column that can have data manual entered
         * foreach end when all input field have be checked for the answer input name
         */

        $list_of_answers = [];
        foreach($request->input('answer', []) as $answer) {
            if($answer != null) {
                $list_of_answers[] = Answer::create([
                    'answer' => $answer,
                ]);
            }
        }

        /*
         * $questions->answer() -> creates a link between question table and answer
         * causing the pivot table to be populated
         * saveMany-> used to call the statement for each item in the array giving it the same question
         * return view is a link to another page to fill out a new question
         * 'questionnaire'-> used to make nsure the questionnaire_id stays same for each question
         * The aim for this return to make it possible to the loop this store function
         */

        $questions->answers()->saveMany($list_of_answers);
        return view('/questionnaire/add_another_question', ['questionnaire'=>$questionnaire_id]);
    }

}
