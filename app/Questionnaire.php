<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model
{
    /*
     * creates the model which links the controller to the questions tables
     * $fillable-> sets out the filed within the table that need to be manual filled in
     */
    protected $fillable = [
        'questionnaire_title',
        'ethics',
        'author_id'
    ];

    /*
     * questions function creates the link to the questions table
     * this tells us that a questionnaire can have many questions
     */
    public function questions(){
        return $this->hasMany('App\Questions');
    }
    public function user(){
        return $this->hasOne('App\User');
    }
}