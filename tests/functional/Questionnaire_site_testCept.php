<?php
$I = new FunctionalTester($scenario);

$I->am('A user');
$I->wantTo('create a new questionnaire');



//This does not test add a new user becuase the code is the same as the tutorial

Auth::loginUsingId(1);
// log in as the user that has been saved in the database from the start
// This is id 1 as it is the only user at the moment.
//// Add a new questionnaire
//
//test to see what can be seen on home page after logging in
$I->amOnPage('/questionnaire/home');
$I->see('See results', 'nav');
$I->see('Logout', 'nav');
$I->dontSee('Login', 'nav');
$I->dontSee('Register', 'nav');

//create a questionnaire
//when
$I->amOnPage('/questionnaire/new_questionnaire');
$I->see('Create a questionnaire', 'h2');
$I->submitForm('#createquestionnaire', [
    'id' => '4',
    'questionnaire_title' => 'questionnaire_2',
    'ethics' => 'ethics_2',
    'author_id' => '1',
]);
$I->see('Create a question', 'h2');

$I->submitForm('#createquestion', [
//    'id' => '2',
//    'questionnaire_id' => '4',
    'question' => 'question_2',
    'answer[]' => 'answer_1',
]);

//make sure that the questionnaire site allows them to add another question
$I->submitForm('#createquestion', [
//    'id' => '2',
//    'questionnaire_id' => '4',
    'question' => 'question_3',
    'answer[]' => 'answer_2',
]);

$I->see('Exit creating questionnaire- make sure you add the questions first', 'A');
$I->click('Exit creating questionnaire- make sure you add the questions first');
//test to see if the button took us back to the home page
$I->see('home page', 'h1');

$I->amOnPage('/questionnaire/home');
$I->see('take part', 'nav');
$I->amOnPage('/questionnaire/questionnaire');
$I->see('Research on the use and the creation of questionnaires');
$I->canSeeLink('Research on the use and the creation of questionnaires');
//the 20% was added after the development due to the fact that I did not understand why it was doing this
$I->amOnPage('/questionnaire/questionnaire/%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%201');
$I->see('Research on the use and the creation of questionnaires', 'h1');
//I want to be able to see all questions
$I->see('What is a questionnaire ?');
$I->see('What is quantitative data');
$I->see('What is Qualitative data');

$I->submitForm('#createresult', [
    'questions_id[]' => '1',
    'answer[]' => 'Both',
]);

//To make sure it has been submitted and re directed to the right page I should be on home
$I->see('Home page', 'h1');
//Test to see if the answer have been sent
$I->amOnPage('/questionnaire/results');
$I->see('Research on the use and the creation of questionnaires');
$I->canSeeLink('Research on the use and the creation of questionnaires');
//the 20% was added after the development due to the fact that I did not understand why it was doing this
$I->amOnPage('/questionnaire/results/%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%201');
$I->see('Research on the use and the creation of questionnaires', 'h1');

//Now i want to be able to see the questions
$I->canSee('what is a questionnaire ?', 'h2');
$I->canSee('What is quantitative data', 'h2');
$I->canSee('What is Qualitative data ', 'h2');

//Now to see if answers can appear

$I->canSee('Both', 'li');


//test finished

