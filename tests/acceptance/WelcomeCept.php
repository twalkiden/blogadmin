<?php 
$I = new AcceptanceTester($scenario);

$I->am('a admin');
$I->wantTo('test Laravel Working');

//when
$I->amOnPage('/');

//then
$I->seeCurrentUrlEquals('/');
$I->see('Laravel 5', '.title');
