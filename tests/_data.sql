-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: localhost    Database: blogadmin
-- ------------------------------------------------------
-- Server version	5.7.17-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article_category`
--

DROP TABLE IF EXISTS `article_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article_category` (
  `article_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  KEY `article_category_article_id_index` (`article_id`),
  KEY `article_category_category_id_index` (`category_id`),
  CONSTRAINT `article_category_article_id_foreign` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `article_category_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article_category`
--

LOCK TABLES `article_category` WRITE;
/*!40000 ALTER TABLE `article_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `article_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `published_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `articles_title_unique` (`title`),
  KEY `articles_published_at_index` (`published_at`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles`
--

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` VALUES (1,'Et ut rerum voluptate corrupti numquam.','Natus ea excepturi molestiae omnis voluptates. Labore id est corrupti quos. Harum ut quis hic velit. Earum omnis possimus sapiente consequatur.','fugit',1,'2017-02-28 13:42:56','2017-02-28 13:42:56','0000-00-00 00:00:00'),(2,'Id quaerat optio quia et consectetur.','Odit nesciunt ut ab expedita voluptatem quisquam. Accusantium quia deleniti rerum aut eius. Odio sint rem hic saepe et quisquam suscipit. Qui sunt provident provident quam corporis.','sit',1,'2017-02-28 13:42:56','2017-02-28 13:42:56','0000-00-00 00:00:00'),(3,'Similique blanditiis officiis dolores non in autem.','Enim error repellat eius aliquam ipsa qui neque. Esse sint harum quod ex. Sit beatae nihil sunt rerum nihil.','atque',1,'2017-02-28 13:42:56','2017-02-28 13:42:56','0000-00-00 00:00:00'),(4,'Id dignissimos quo sed rerum.','Voluptas itaque dolorum sed adipisci rerum. Tenetur est est et facere consequatur ea aut et. Nihil quibusdam numquam beatae sunt. Qui aut repellendus molestiae et voluptas minus inventore.','tempore',1,'2017-02-28 13:42:56','2017-02-28 13:42:56','0000-00-00 00:00:00'),(5,'Beatae quo voluptas velit ullam at et ducimus suscipit.','Quis ducimus sint accusantium dolorum iure cum cumque. Eius mollitia repudiandae optio qui commodi. Eum vel neque nihil odit odio minima nostrum. Laborum distinctio maxime possimus. Eius eos maxime nihil ipsa et sed illum.','alias',1,'2017-02-28 13:42:56','2017-02-28 13:42:56','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `detail` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_title_unique` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'java','java',NULL,NULL),(2,'Python','python',NULL,NULL),(3,'javascript','javascript',NULL,NULL),(9999,'Randomtest','a test category',NULL,NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2016_02_19_100050_create_roles_table',1),('2016_02_19_100108_create_permissions_table',1),('2016_02_19_100123_create_articles_table',1),('2016_02_19_100137_create_categories_table',1),('2016_02_19_100200_create_role_user_table',1),('2016_02_19_100219_create_permission_role_table',1),('2016_02_19_100236_create_article_category_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`user_id`),
  KEY `role_user_user_id_foreign` (`user_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Timothy Walkiden','twakiden@example.com','$2y$10$jL9kTQqKHofnpHnlB6uU4OT.vckhC6Q64NA90nSznaRumgEJRo6L6','FDYr5WFvLR',NULL,NULL),(2,'Eduardo D\'Amore II','gertrude80@example.com','$2y$10$gFrT2jGoyxIIEqA/2ju66.goSgmg3HB/h2iCbPDF4IBrb53EFtYbm','JHzKcFmZ4G','2017-02-28 13:42:54','2017-02-28 13:42:54'),(3,'Christophe Gislason','mikel13@example.com','$2y$10$dJHwwHGFjwGGozXvo7l7GeSkLua0lfkOalgHpdyvppgEF93BwoNYC','VBQg1ztfQh','2017-02-28 13:42:55','2017-02-28 13:42:55'),(4,'Salvatore Kreiger','kshlerin.mariana@example.com','$2y$10$lvdXYAiu3ZtjIYgO2P9wJuge.NXHbtDz2xF6j4qlPpPXCUEgjfr92','LGH5asW1h8','2017-02-28 13:42:55','2017-02-28 13:42:55'),(5,'Randy Hilll II','antonetta59@example.org','$2y$10$II9fs0IFHsgwFZqAZP6VOubSVKmxkhSUq2lHlVLnuGQUryq5tVdZS','QXM02CcwkR','2017-02-28 13:42:55','2017-02-28 13:42:55'),(6,'Nola Tremblay','mohammad60@example.net','$2y$10$VUGRkHFNCoVZ0Zi/esBnGuy8UfpeqYBBWxShEFRYxiEVyv.b9jITC','mzyIMEHp0K','2017-02-28 13:42:55','2017-02-28 13:42:55'),(7,'Mr. William Orn','dibbert.carole@example.net','$2y$10$9kX9uJcfllx3XGsh7vLP9ew/1WbWYZWxW0.IUfRS9LioRWx/bVpVm','eLzFWErQLw','2017-02-28 13:42:55','2017-02-28 13:42:55'),(8,'Rachel Maggio','knolan@example.net','$2y$10$9/YFdpPLXTima35B7ZzTi.io6em.v7XfUz8f8C0fOZHuz.INzxTja','9dRP3pqsAn','2017-02-28 13:42:55','2017-02-28 13:42:55'),(9,'Prof. Isaiah Schaden V','arthur89@example.org','$2y$10$mqM/7.bWzI3lsyFPNaVyFOB1B00WljYzapc01L09oRqiJuSnT719K','wwi85QDn3s','2017-02-28 13:42:55','2017-02-28 13:42:55'),(10,'Ms. Bernita Langosh','bhaley@example.com','$2y$10$8TpCbRfZrBpQLgkQr6jH0uNT8DcD1PYRASzpXbirS9wFPn2GYpFFG','fCEfKvLhia','2017-02-28 13:42:55','2017-02-28 13:42:55'),(11,'Liza Conn','konopelski.crawford@example.net','$2y$10$RmNdj/9ZfSDGI29CnNmTNO4l8NiHfmYO9LNMcHNYYS2sKDuHDBIaO','8O7SWdimlw','2017-02-28 13:42:55','2017-02-28 13:42:55'),(12,'Stanley Stehr','hermann.conn@example.net','$2y$10$GtuPTJ89LGFF7sIrZUbDaORApdNKewFPOYkca6qfjKumo7e4vuhwi','i9rogvmgQq','2017-02-28 13:42:55','2017-02-28 13:42:55'),(13,'Aiden Walsh','missouri45@example.org','$2y$10$v4QcahJDMc6cJt/6f1Mnk.DMYJ0UwKqm5B/r8k/2z2EIXNv4DGCCG','bCzJEgr1bg','2017-02-28 13:42:55','2017-02-28 13:42:55'),(14,'Scotty Miller I','iquigley@example.org','$2y$10$ZRw582XgcCcseU0yCRdZwOGHA6OrLiyZT4oUbbOmuN8Wg45SdQYyC','TjkZOYTh38','2017-02-28 13:42:55','2017-02-28 13:42:55'),(15,'Granville Romaguera','lrogahn@example.org','$2y$10$h1sc5gpM6kY8OdzWZvWtJudTVsVcKuWtz4gFGg3NllsHgJHBf8yEO','l6Iry8EAgY','2017-02-28 13:42:55','2017-02-28 13:42:55'),(16,'Zora Rosenbaum','stokes.kirstin@example.net','$2y$10$ANrkjjQpm/amml9.OOFLIuDFnbiAtoCFqSBHB7.hUm2bHEm9dZp7W','3n4dEvF0Nm','2017-02-28 13:42:55','2017-02-28 13:42:55'),(17,'Delfina Bogan','lvon@example.net','$2y$10$TKP0iXGq7qBPwIWrmL1xquEYfjBVocMAyv5l.Sdd5KzaEcpcTe/Yq','C3KTfeqlv5','2017-02-28 13:42:55','2017-02-28 13:42:55'),(18,'Regan Turcotte','kaden57@example.net','$2y$10$Bk9ylWUw.hI9aYq3Ir3LZuvjyb0JJFkDqsN8LkD9N913NJTioMY9a','GRlyBhcUnS','2017-02-28 13:42:55','2017-02-28 13:42:55'),(19,'Everett Stracke','nolan.ernestine@example.net','$2y$10$KH8Wvmp7kly7M82ruuUQ.eOG8N4eBeYrTF6Ts5iauk56kW06em0nO','mZMxpAc1i5','2017-02-28 13:42:55','2017-02-28 13:42:55'),(20,'Alejandra Schaefer','novella.padberg@example.org','$2y$10$vVFlyRvPAekAJc9on5GToOsojd3qztN.xTG/0YM9B7uA3.TV9nWWe','pI7hKq7ySb','2017-02-28 13:42:55','2017-02-28 13:42:55'),(21,'Lorine Kilback','davonte92@example.com','$2y$10$B81JULoXOmn3qJlHqcimpuAKqrfeVJYcBqMJljcDJFGOAX4tiuCOO','Kwg9WQPDCc','2017-02-28 13:42:55','2017-02-28 13:42:55'),(22,'Philip Herman','cweimann@example.net','$2y$10$lWRK6/tsJjCnEI0yvwM3Ke9U58HDJDBljxxCDltIprmPkxzzWlgXO','VENR6PncRK','2017-02-28 13:42:55','2017-02-28 13:42:55'),(23,'Kirsten Greenholt','antonina.rice@example.com','$2y$10$yfCPlG0Oy8DaQe2FZVhhtOB44IqH2Ls8P29MvUyKzTmoLcqRFVW4W','p0yu0qtHYB','2017-02-28 13:42:55','2017-02-28 13:42:55'),(24,'Alena Rippin','juston.hyatt@example.net','$2y$10$WJAYXoNvJiGkPKkarnKPdOP9RcSHKRC6i0QUM/Xz4akE3nhRIiK/K','ThxH4kFvbN','2017-02-28 13:42:55','2017-02-28 13:42:55'),(25,'Prof. Charley Klocko MD','caesar.bayer@example.org','$2y$10$1S0iFuNxegcpXEvD/JI7q.oZ8AsBy4JFyNQqmv4fevrHNwnw5oetW','mjFrRqwHxX','2017-02-28 13:42:55','2017-02-28 13:42:55'),(26,'Dorian Schroeder','reilly.joanie@example.net','$2y$10$Qvetdtl6mWTPPi/7pGiyhenL3Bn058vO.AVSI1dnTz9Cbngm7cZki','lxRt3qlXeY','2017-02-28 13:42:55','2017-02-28 13:42:55'),(27,'Raleigh Adams','wunsch.kameron@example.org','$2y$10$5/4MS.8v3ts/YZGPaZBjh.tMwCYVsGNyD9SYXvALl9.MJiBuvrRXC','djz7U0b9YN','2017-02-28 13:42:55','2017-02-28 13:42:55'),(28,'Lamont Nicolas','timmothy.kemmer@example.com','$2y$10$tr9p2fq.w9Vl/6OzmHuz6Ov5dWRVfOMlmPlEcVDkxp.CNqRQ2oY0W','awRUJSA8BJ','2017-02-28 13:42:55','2017-02-28 13:42:55'),(29,'Greta Wisoky','adelia31@example.com','$2y$10$CaAnpg6bTsKAlZiYbsrqnuxUmK9cJw43.LCA8bRCY5pHn7omqzMqi','p3PsRnzVME','2017-02-28 13:42:55','2017-02-28 13:42:55'),(30,'Elenor Hagenes','fannie18@example.net','$2y$10$Modtj2GdmgEAl5qk1mghkOJScYcP/1X.IHqArt0wD/zYbdZkl6fx.','P5D7rkOCKF','2017-02-28 13:42:55','2017-02-28 13:42:55'),(31,'Glen Koch','kunde.merlin@example.com','$2y$10$DkKctecpffgu/c8m467rE.Oke2qz0vpqVsnrnxyfh.R5N4zzNYYHS','7ZS8srnPzB','2017-02-28 13:42:55','2017-02-28 13:42:55'),(32,'Madisyn Erdman','jarvis06@example.com','$2y$10$6fuiaUQmbMOsjGU9gGkRU.P.5Ukfrw1svdR9MYHFMs/y0tsMcbyCi','eirIi94nVn','2017-02-28 13:42:55','2017-02-28 13:42:55'),(33,'Kattie Hudson','dhodkiewicz@example.org','$2y$10$YzSuNBSiGl5vBliXlZ8ZcOr/JbON8wcP1nEt8Dx6.fjjELxXktNLS','WsK5XZ6oYy','2017-02-28 13:42:55','2017-02-28 13:42:55'),(34,'Mrs. Assunta Ferry PhD','huels.domenick@example.net','$2y$10$YhwfHbZcHwMd.jRZ5TO.ne0OSrH.3qOflrZrpElOwaIHJ1hAXVC3u','517MAS2wi7','2017-02-28 13:42:55','2017-02-28 13:42:55'),(35,'Armando Zemlak PhD','delphine.cartwright@example.net','$2y$10$rFcqTLqKW886s/r6jhTnZe6I2mWmSHFLROp2drcJBqYXFXqFvHoyy','KBBsl8dJYp','2017-02-28 13:42:55','2017-02-28 13:42:55'),(36,'Drake Kris','turcotte.annamarie@example.net','$2y$10$nnDUYrfc3wB8wMRTJIjQa.Iq/eFgwZUKDfEvZrixnqLhp.UH0E4j.','X8xmuBOv0P','2017-02-28 13:42:55','2017-02-28 13:42:55'),(37,'Ms. Ena Bartell Sr.','hmosciski@example.net','$2y$10$K2IA.AGcYJCDRsCI1Le1O.Yz5pqThX7lMRWrca7BniOsc.y1a5OqG','2s2sMxeK0H','2017-02-28 13:42:55','2017-02-28 13:42:55'),(38,'Miss Jennie Bednar','jasen.pfannerstill@example.net','$2y$10$xmGfv1kc3aYI9EB9ZsKFr.LJeC4veTV4HOrQkSo249xRIpnINz1iW','cydCxZo2RM','2017-02-28 13:42:55','2017-02-28 13:42:55'),(39,'Devon Strosin','epfeffer@example.net','$2y$10$6u9BmUCnSAagEDPH9f1PYuEelSZ0jAGhYEJ8T5DBsirR7UPzGQ18C','8aKo2vQfZc','2017-02-28 13:42:55','2017-02-28 13:42:55'),(40,'Ines Zboncak','clotilde29@example.com','$2y$10$XGS9BOAxGIfXquPJw.tGs.RcFjBv6XwNy72e9DS25MU0tM7vO4gzS','YCLBbIk44i','2017-02-28 13:42:55','2017-02-28 13:42:55'),(41,'Mckenna Prohaska','pansy.king@example.net','$2y$10$biMjsO4Lx2hUB0A90iuDked/ckSFiDmPktH4UOD6CHsmGkTFrRbNu','kj0yrtIkN3','2017-02-28 13:42:56','2017-02-28 13:42:56'),(42,'Malachi Christiansen DVM','junior.erdman@example.org','$2y$10$NSg649CIAjWfS3XMJqqtceWEM6ZrAH4A9gxqXQ9QztS94l6rdG7GW','kajyVvWtVk','2017-02-28 13:42:56','2017-02-28 13:42:56'),(43,'Braden Nienow DVM','breanne.romaguera@example.com','$2y$10$n7GUSZrvAJXOLOVF.XkicuqDpHBljLNU9eiZ49p7snoix2EcLlvxa','ogEaOCe58a','2017-02-28 13:42:56','2017-02-28 13:42:56'),(44,'Miss Mazie Eichmann','hugh61@example.com','$2y$10$t9qRKnthZvXTzkszcgUdPODP/MsXxobQgmdGc5UyRvaDB4qyandTq','DOx5AGl326','2017-02-28 13:42:56','2017-02-28 13:42:56'),(45,'Easton Hilpert III','qlubowitz@example.net','$2y$10$24vKD2Z2ST4urNP61XtA5uw0QQNsklMpDof61yvlZb6SxcNj7.d4y','wjGqScWC6v','2017-02-28 13:42:56','2017-02-28 13:42:56'),(46,'Solon Ullrich','tremblay.reyes@example.com','$2y$10$V8XaOfYTYJvIPd1G.pYyq.a7LjALxWdCOzeClA7sRLYr2ANS6zR3.','5Ag3Qel00d','2017-02-28 13:42:56','2017-02-28 13:42:56'),(47,'Rhea Okuneva','marquardt.dangelo@example.org','$2y$10$bQaE73zswf.vD1Mjp7cKaumlz2tdFWwFwyjPboSjvJiiF/wSDud3C','6XNDmeU2I7','2017-02-28 13:42:56','2017-02-28 13:42:56'),(48,'Dr. Zoila Huels','stracke.maymie@example.com','$2y$10$.eLxlr8QHMwqCDcGKwQMoukXLeOFew5xgfAMmnXQXHZ2rITnfRAwi','glNPbz3Wbn','2017-02-28 13:42:56','2017-02-28 13:42:56'),(49,'Alyce O\'Connell','uokuneva@example.org','$2y$10$kQcZdWQEGTh/en..Be9Qcue/qMjhQWE93Ue1iJZxzr1q44tELhfGy','qaCmeWnkTh','2017-02-28 13:42:56','2017-02-28 13:42:56'),(50,'Shannon Bogisich','kelli.hudson@example.net','$2y$10$L9BssJGFM.4IP1GdHemWJuIer7rFKJYfYoMy3owdRpc1.qbDNeSPy','5rOugrZqbx','2017-02-28 13:42:56','2017-02-28 13:42:56'),(51,'Ms. Ines Gleason','giovanny12@example.net','$2y$10$LMpWhyAFvUav1zJfTqYhF.Gs79ZlB028ggG.gSekd7SuMDYWSF/f2','RkXj2ucJx2','2017-02-28 13:42:56','2017-02-28 13:42:56');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-28 13:49:25
