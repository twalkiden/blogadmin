<?php 
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('create a new role');
Auth::loginUsingId(1);

// When
$I->amOnPage('/admin/role');
$I->see('New Role', 'title');
$I->dontSee('Publisher');
// And
$I->click('Add Role');

// Then
$I->amOnPage('/admin/users/create');
// And
$I->see('Add role', 'h1');
$I->submitForm('#createrole', [
    'name' => 'Publisher',
    'Label' => 'Publisher',

]);
// Then
#$I->seeCurrentUrlEquals('/admin/users');
#$I->see('Role', 'h1');
#$I->see('New role added!');
#$I->see('Publisher');
