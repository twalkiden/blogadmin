var blogApp= angular.module('blogApp', ['ngRoute', 'CategoryControllers', 'categoryService']);
blogApp.config(['$routeProvider',
    function($routeProvider){
        $routeProvider.
            when('/blogs',{
                templateUrl: 'scripts/views/categorylist.html',
                controller: 'CategoryControllersr'
            }).
            when('/blog/:id',{
            templateUrl: 'scripts/views/blog.html',
            controller: 'CategoryControllers'
            }).
            otherwise({
                redirectTo: '/category'
        });

}]);
