var blogApp= angular.module('blogApp', ['ngRoute', 'BlogControllers', 'blogService']);
blogApp.config(['$routeProvider',
    function($routeProvider){
        $routeProvider.
            when('/blogs',{
                templateUrl: 'scripts/views/list.html',
                controller: 'blogController'
            }).
            when('/blog/:id',{
            templateUrl: 'scripts/views/blog.html',
            controller: 'articleController'
            }).
            otherwise({
                redirectTo: '/blogs'
        });

}]);
