<?php

use Illuminate\Database\Seeder;

class createcategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            ['id' => 1, 'title' => "java", 'detail' => "java"],
            ['id' => 2, 'title' => "Python", 'detail' => "python"],
            ['id' => 3, 'title' => "javascript", 'detail' => "javascript"],
        ]);
    }
}
