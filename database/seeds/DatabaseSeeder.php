<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Category;
use App\Article;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);



      //  DB::statement('SET FOREIGN_KEY_CHECKS=0;');
      /*  Model::unguard();

        Category::truncate();
        User::truncate();
        Article::truncate();

        Model::reguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');*/

        $this->call(CategoriesTableSeeder::class);
        $this->call(UserTableSeeder::class);

        factory(User::class, 50)->create();
        factory(Article::class, 5)->create();







        $this->call(createcategoriesSeeder::class);
    }
}
