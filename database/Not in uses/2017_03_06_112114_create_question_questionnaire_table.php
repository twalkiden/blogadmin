<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionQuestionnaireTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question__questionnaires', function (Blueprint $table) {
            $table->integer('questionnaire_id')->unsigned();
            $table->integer('question_id')->unsigned()->nullable();
            $table->foreign('questionnaire_id')->references('id')->on('questionnaires');
            $table->foreign('question_id')->references('id')->on('questions');

            //$table->primary(['question_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('question__questionnaires');
    }
}
