<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultsTable extends Migration
{
    /**
     * function up-> creates the results table
     * answer-> stores the answer which has been selected from the list of answers
     * questions_id is used to store which question the result belongs to
     */
    public function up()
    {
        Schema::create('results', function (Blueprint $table) {
            $table->increments('id');
            $table->text('answer');
            $table->integer('questions_id')->unsigned();
            $table->foreign('questions_id')->references('id')->on('questions');
            $table->timestamps();
        });
    }

    /**
     * function down used to clear the data from the database when refreshing the table
     */
    public function down()
    {
        Schema::drop('results');
    }
}
