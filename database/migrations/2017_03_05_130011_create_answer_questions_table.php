<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswerQuestionsTable extends Migration
{
    /**
     * function up is used to create a answer_question pivot table
     * a table allows a many to many relationship
     * questions_id-> get the id from the question
     * answer_id-> get the id from the answer question
     */
    public function up()
    {
        Schema::create('answer_questions', function (Blueprint $table) {
            $table->integer('questions_id')->unsigned();
            $table->integer('answer_id')->unsigned();
            $table->foreign('questions_id')->references('id')->on('questions');
            $table->foreign('answer_id')->references('id')->on('answers');

            $table->primary(['questions_id','answer_id']);
        });
    }

    /**
     * function down used to clear the data from the database when refreshing the table
     */
    public function down()
    {
        Schema::drop('answer_questions');
    }
}
