<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswersTable extends Migration
{
    /**
     *up function->creates the answer table
     * id-> used to be able to link to the answer_questions pivot table
     */
    public function up()
    {
        Schema::create('answers', function (Blueprint $table) {
            $table->increments('id');
            $table->text('answer');
            $table->timestamps();

           // $table->primary(['id']);

        });
    }

    /**
     * function down used to clear the data from the database when refreshing the table
     */
    public function down()
    {
        Schema::drop('answers');
    }
}
