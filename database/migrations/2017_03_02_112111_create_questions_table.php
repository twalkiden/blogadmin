<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * function up is used to create the questions
     * id-> used as a primary key for the answer/results tables
     * questionnaire_id is used to link this table to the questionnaire table
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('questionnaire_id')->unsigned();
            $table->text('question');
            $table->timestamps();

            $table->foreign('questionnaire_id')->references('id')->on('questionnaires');


//            $table->primary(['id']);
        });
    }

    /**
     * function down used to clear the data from the database when refreshing the table
     */
    public function down()
    {
        Schema::drop('questions');
    }
}
