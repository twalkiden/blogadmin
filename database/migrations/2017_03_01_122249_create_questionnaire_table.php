<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionnaireTable extends Migration
{
    /*
     * creates the questionnaire table to store the questionnaire
     * function up is used to create the columns within the database
     * id-> is used as a primary key so the questions table can get information on the questionnaire
     * ethics-> used to store ethics statement
     * author_id is used to link this table to the user table so we can see who created the questionnaire
     */
    public function up()
    {
        Schema::create('questionnaires', function (Blueprint $table) {
            $table->increments('id');
            $table->text('questionnaire_title');
            $table->text('ethics');
            $table->integer('author_id')->unsigned();
            $table->foreign('author_id')->references('id')->on('users');
            $table->timestamps();


//            $table->primary(['id']);
        });
    }

    /**
     * function down used to clear the data from the database when refreshing the table
     */
    public function down()
    {
        Schema::drop('questionnaire');
    }
}
